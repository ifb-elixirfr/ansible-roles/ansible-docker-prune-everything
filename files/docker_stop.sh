#!/bin/bash

#set -x


docker_cur_id=$(docker ps | grep $(hostname) | cut -f1 -d' ')
echo "==== I am $docker_cur_id ===="

#docker_cur_id=$(docker inspect -f "{{.Id}}" $(hostname))
#docker_cur_id=$(cat /proc/self/cgroup | grep docker | cut -f3 -d'/'| sed -e s/dcoker.//g | sed -e s/.scope//g | sort -u)

#$(cat /proc/self/cgroup | grep "docker" | sed 's/\([0-9]\):.*:\/docker\///g')

docker_id_list=$(docker ps -a -q | grep -v "$docker_cur_id")
#echo $docker_id_list

for i in $docker_id_list
do
    echo "==== Stoping $i ===="
    docker stop  $i
done
