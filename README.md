Role Name
=========

A role to prune all docker garbage

https://docs.ansible.com/ansible/devel/modules/docker_prune_module.html

https://docs.docker.com/config/pruning/

Example Playbook
----------------

```yaml
- hosts: hosts-to-prune
  roles:
    - docker-prune-everything
```

License
-------

MIT

